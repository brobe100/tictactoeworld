package com.briaro.tictactoeworld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

public class CustomGameActivity extends Activity implements OnItemSelectedListener {

	private String firstPlayerSelection;	//Used to store a string indicating the first player selection.
	private String xPlayerSelection;	//Used to store the X player selection, either user or computer.
	private String oPlayerSelection;	//Used to store the O player selection, either user or computer.
	private int xComputerLevel;			//Used to store the selected level for the X computer.  A zero value means automatic level selection.
	private int oComputerLevel;			//Used to store the selected level for the O computer.  A zero value means automatic level selection.
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.custom_game_options);
		
		Spinner spinnerFirstPlayer = (Spinner) findViewById(R.id.custom_game_first_player_spinner);	//Get first player selection spinner.
		spinnerFirstPlayer.setOnItemSelectedListener(this);	//And add a listener.
		
		Spinner spinnerXPlayer = (Spinner) findViewById(R.id.custom_game_x_player_spinner);	//Get X player selection spinner.
		spinnerXPlayer.setOnItemSelectedListener(this);	//And add a listener.
		
		Spinner spinnerOPlayer = (Spinner) findViewById(R.id.custom_game_o_player_spinner);	//Get O player selection spinner.
		spinnerOPlayer.setOnItemSelectedListener(this);	//And add a listener.
		
		Spinner spinnerXLevel = (Spinner) findViewById(R.id.custom_game_x_level_spinner);	//Get X computer level spinner.
		spinnerXLevel.setOnItemSelectedListener(this);	//And add a listener.
		
		Spinner spinnerOLevel = (Spinner) findViewById(R.id.custom_game_o_level_spinner);	//Get O computer level spinner.
		spinnerOLevel.setOnItemSelectedListener(this);	//And add a listener.
	}
	
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		switch (parent.getId()) {						//See which spinner was clicked.
		case R.id.custom_game_first_player_spinner:
			if (position == 0)	//First player is random
				firstPlayerSelection = GameActivity.FIRST_PLAYER_RANDOM;	//So set the first player as random.
			else if (position == 1)	//First player is X
				firstPlayerSelection = GameActivity.FIRST_PLAYER_X;	//So set the first player as X.
			else	//First player is O
				firstPlayerSelection = GameActivity.FIRST_PLAYER_O;	//So set the first player as O.
			break;
		case R.id.custom_game_x_player_spinner:				//X player selection spinner was clicked
			if (position == 0)									//1st selection is user
				xPlayerSelection = GameActivity.PLAYER_USER;		//So set the X player to the user
			else												//2nd selection is computer
				xPlayerSelection = GameActivity.PLAYER_COMPUTER;	//so set the X player to the computer
			break;
		case R.id.custom_game_o_player_spinner:				//O player selection spinner was clicked
			if (position == 0)									//1st selection is user
				oPlayerSelection = GameActivity.PLAYER_USER;		//So set the O player to the user
			else												//2nd selection is computer
				oPlayerSelection = GameActivity.PLAYER_COMPUTER;	//so set the O player to the computer
			break;
		case R.id.custom_game_x_level_spinner:				//X level spinner selection
				xComputerLevel = position;						//Set the X computer level to the position, which corresponds the the level selected.  Zero means automatic level selection.
			break;
		case R.id.custom_game_o_level_spinner:				//O level spinner selection
				oComputerLevel = position;						//Set the O computer level to the position, which corresponds the the level selected.  Zero means automatic level selection.
			break;

		}
	}
		
	public void onNothingSelected(AdapterView<?> parent) {
		// Another interface callback     
	}
		
	public void startGameButtonClicked(View view) {
	    Intent intent = new Intent(this, GameActivity.class);
	    Bundle bundle = new Bundle();
	    bundle.putString(GameActivity.FIRST_PLAYER, firstPlayerSelection);	//Set the first player to selection.
	    bundle.putString(GameActivity.X_PLAYER, xPlayerSelection);	//Set X player to selection.
	    bundle.putString(GameActivity.O_PLAYER, oPlayerSelection);	//Set O player to selection
	    bundle.putInt(GameActivity.X_COMPUTER_LEVEL, xComputerLevel);	//Set X computer level.  Passing a zero means to automatically set the level.
	    bundle.putInt(GameActivity.O_COMPUTER_LEVEL, oComputerLevel);	//Set O computer level.  Passing a zero means to automatically set the level.
	    intent.putExtras(bundle);
	    startActivity(intent);
		}
}
