package com.briaro.tictactoeworld;



/**
 * This class provides a user interface for the tic-tac-toe game.  It reports
 * which square in the tic-tac-toe board the user clicked.
 * @author Brian Roberts
 *
 */
public class UserInterface {
	
	//Mouse click listener code.
	private boolean waitingForUserClick = false;
	private boolean userClicked = false;
	private int column, row;
	
	/**
	 * Called to let this class know that the main code is waiting for the user
	 * to click.
	 */
	public void waitingForUser() {
		if (userClicked == false)	//If the user hasn't clicked yet
			waitingForUserClick = true;	//Then set the flag to wait for user.
	}
		
	/**
	 * @return	Returns true if the user clicks a square, else it returns false.
	 */
	public boolean userClicked() {
		boolean returnedValue = userClicked;	//Return true if the user clicked, else return false.
		userClicked = false;					//Since the information that the user clicked is being returned, clear out the user clicked flag.
		return returnedValue;					//Return true if the user clicked, else return false.
	}
		
	/**
	 * @return	Returns the last column in the tic-tac-toe board the user clicked.
	 */
	public int getUserSelectedColumn() {
		return column;
	}
		
	/**
	 * @return	Returns the last row in the tic-tac-toe board the user clicked.
	 */
	public int getUserSelectedRow() {
		return row;
	}

	/**
	 * Updates the tic tac toe row and column that the user clicked.
	 * @param userTouchx	Where the used touched on the bitmap, not the screen.  This should be passed at the original bitmap scale.
	 * @param userTouchy	Where the used touched on the bitmap, not the screen.  This should be passed at the original bitmap scale.
	 */
	public void getRowAndColumnOfUserTouch(int userTouchx, int userTouchy) {
		if (waitingForUserClick) {
			
		    if (userTouchx < Gameboard.getColumnCoordinate(0))
		    	column = 0;
		    else if (userTouchx < Gameboard.getColumnCoordinate(1))
		    	column = 1;
		    else
		    	column = 2;
		    
		    if (userTouchy < Gameboard.getRowCoordinate(0))
		    	row = 0;
		    else if (userTouchy < Gameboard.getRowCoordinate(1))
		    	row = 1;
		    else
		    	row = 2;
		    
		    userClicked = true;	//Update flag indicating that the user clicked.
			waitingForUserClick = false;
		}
	}

}
