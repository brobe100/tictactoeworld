package com.briaro.tictactoeworld;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;


/**
 * This class creates a tic-tac-toe board to display.
 * @author Brian Roberts
 *
 */
/**
 * @author Brian
 *
 */
public class Gameboard {
	
	private static int[] columnCoordinate = new int[2];	//Horizontal pixel coordinates for each column.  Used to determine which square was clicked.
	private static int[] rowCoordinate = new int[2];	//Vertical pixel coordinates for each row.  Used to determine which square was clicked.
	private static int[] markerColumnCoordinate = new int[3];	//Horizontal pixel coordinates for each column for markers.  Used to determine where to place the marker images.
	private static int[] markerRowCoordinate = new int[3];	//Vertical pixel coordinates for each row for markers.  Used to determine where to place the marker images.
	private final static int boardSize = 3;	//Number of squares for the board.  Check all code in this class before changing.

	//Activity used to get resources for layout and images.
	public static Activity activity;
	
	private char gameBoardState[][] = new char[boardSize][boardSize];	//Holds the markers (X, O, N) for the game board.
	private int lastMoveColumn = 0, lastMoveRow = 0;					//Holds the column and row for the last move.
	
	private Bitmap ticTacToeBoardBitmap;	//Image used for the blank tic tac toe board.
	private Bitmap xImage;					//Image used for the X marker.
	private Bitmap oImage;					//Image used for the O marker.
	private Bitmap xWinRowImage;			//Image for the line across a row of X's.
	private Bitmap xWinColumnImage;			//Image for the line down a column of X's.
	private Bitmap xWinDiag0Image;			//Image for the line from top-left to bottom-right of X's.
	private Bitmap xWinDiag1Image;			//Image for the line from top-right to bottom-left of X's.
	private Bitmap oWinRowImage;			//Image for the line across a row of O's.
	private Bitmap oWinColumnImage;			//Image for the line down a column of O's.
	private Bitmap oWinDiag0Image;			//Image for the line from top-left to bottom-right of O's.
	private Bitmap oWinDiag1Image;			//Image for the line from top-right to bottom-left of O's.
	
	/**
	 * Used to draw the line showing the winner.
	 * @author Brian
	 *
	 */
	private class Win{
		char marker = 'N';	//Indicates the winner, X or O.  'N' means no winner.
		String type;	//Indicates the win type. "ROW", "COLUMN", "DIAG0", or "DIAG1".  "DIAG0" is from top-left to bottom-right.  "DIAG1" is from top-right to bottom-left.
		int position;	//Indicates which row or column to display the line.  The top row is row 0, the left column is column 0.  This parameter does nothing for diagonal lines.
	}
	private Win win = new Win();	//Create the actual win object.
	
	/**
	 * Creates a new game board.
	 */
	public Gameboard(Activity _activity){
		Gameboard.activity = _activity;	//Set activity to be the same as the calling activity.
		Bitmap tic_tac_toe_board_bitmap = BitmapFactory.decodeResource(Gameboard.activity.getResources(), R.drawable.tic_tak_toe_board);	//Gets the bare tic tac toe board bitmap.
		Bitmap xImage = BitmapFactory.decodeResource(Gameboard.activity.getResources(), R.drawable.x);	//Gets the X marker bitmap.
		Gameboard.setColumnCoordinates(tic_tac_toe_board_bitmap);	//Sets the column coordinates based on the size of the tic tac toe board image.  Used to determine which square was clicked.
		Gameboard.setRowCoordinates(tic_tac_toe_board_bitmap);	//Sets the row coordinates based on the size of the tic tac toe board image.  Used to determine which square was clicked.
		Gameboard.setMarkerColumnCoordinates(xImage);	//Sets the column coordinates based on the size of the tic tac toe board image.
		Gameboard.setMarkerRowCoordinates(xImage);	//Sets the row coordinates based on the size of the tic tac toe board image.
    	newGame();
 	}
	
	/**
	 * Used to copy one game board to another new game board instance.
	 * @param another	The game board to copy.
	 */
	public Gameboard(Gameboard another) {
		for (int row = 0; row < boardSize; row++) {
	    	for (int column = 0; column < boardSize; column++) {
	    		this.gameBoardState[column][row] = another.gameBoardState[column][row];
	    	}
	    }
	}
	
	/**
	 * Sets the column coordinates based on the size of the tic tac toe board
	 * image.  For a three column board, the first coordinate is set to 1/3 of
	 * the width of the image.  For a three column board, the second coordinate
	 * is set to 2/3 of the width of the image.  Used to determine which square
	 * was clicked.
	 * @param tic_tac_toe_board_bitmap	Image to set the column coordinates from.
	 */
	private static void setColumnCoordinates(Bitmap tic_tac_toe_board_bitmap) {
		columnCoordinate[0] = tic_tac_toe_board_bitmap.getWidth() / 3;	//The first coordinate is set to 1/3 of the width of the image.
		columnCoordinate[1] = columnCoordinate[0] * 2;					//The second coordinate is set to 2/3 of the width of the image.
	}
	
	/**
	 * Sets the row coordinates based on the size of the tic tac toe board
	 * image.  For a three row board, the first coordinate is set to 1/3 of the
	 * height of the image.  For a three row board, the second coordinate is
	 * set to 2/3 of the height of the image.  Used to determine which square
	 * was clicked.
	 * * @param tic_tac_toe_board_bitmap	Image to set the row coordinates from.
	 */
	private static void setRowCoordinates(Bitmap tic_tac_toe_board_bitmap) {
		rowCoordinate[0] = tic_tac_toe_board_bitmap.getHeight() / 3;	//The first coordinate is set to 1/3 of the height of the image.
		rowCoordinate[1] = rowCoordinate[0] * 2;					//The second coordinate is set to 2/3 of the height of the image.
	}
	
	/**
	 * Sets the column coordinates for the markers.  The coordinates are for
	 * the left side of the markers, and it will center the markers in the
	 * square.
	 * @param markerImage	Marker image to use.
	 */
	private static void setMarkerColumnCoordinates(Bitmap markerImage) {
		int squareWidth = columnCoordinate[0];					//Width of a square.
		int markerWidth = markerImage.getWidth();				//Width of a marker.
		int columnCenterCoordinate = columnCoordinate[0] / 2;	//Center of the first column.

		markerColumnCoordinate[0] = columnCenterCoordinate - (markerWidth / 2);	//Coordinate for left marker.
		markerColumnCoordinate[1] = markerColumnCoordinate[0] + squareWidth;	//Coordinate for center marker.
		markerColumnCoordinate[2] = markerColumnCoordinate[1] + squareWidth;	//Coordinate for right marker.
	}
	
	/**
	 * Sets the row coordinates for the markers.  The coordinates are for the
	 * top side of the markers, and it will center the markers in the square.
	 * @param markerImage	Marker image to use.
	 */
	private static void setMarkerRowCoordinates(Bitmap markerImage) {
		int squareHeight = rowCoordinate[0];			//Height of a square.
		int markerHeight = markerImage.getHeight();		//Height of a marker.
		int rowCenterCoordinate = rowCoordinate[0] / 2;	//Center of the first row.

		markerRowCoordinate[0] = rowCenterCoordinate - (markerHeight / 2);	//Coordinate for top marker.
		markerRowCoordinate[1] = markerRowCoordinate[0] + squareHeight;		//Coordinate for center marker.
		markerRowCoordinate[2] = markerRowCoordinate[1] + squareHeight;		//Coordinate for bottom marker.
	}
	
	/**
	 * @param columnLine	Requested vertical line to return.
	 * @return	Returns the coordinate for the requested vertical line of the board.
	 */
	public static int getColumnCoordinate(int columnLine) {
		return columnCoordinate[columnLine];
	}
	
	/**
	 * @param columnLine	Requested horizontal line to return.
	 * @return	Returns the coordinate for the requested horizontal line of the board.
	 */
	public static int getRowCoordinate(int rowLine) {
		return rowCoordinate[rowLine];
	}
	
	/**
	 * Resets the game board and redraws it.
	 */
	public void newGame() {
		for (int row = 0; row < boardSize; row++) {
	    	for (int column = 0; column < boardSize; column++) {
	    		gameBoardState[column][row] = 'N';
	    	}
	    }
		win.marker = 'N';	//Reset the winner to none.
		//drawBoard();
	}
	
	/**
	 * Updates a square on the game board, and then redraws the board.
	 * @param column	The column to place the marker.
	 * @param row	The row to place the marker.
	 * @param marker	Which marker to place.  Should be either 'X' or 'O'.
	 */
	public void updateGameSpace(int column, int row, char marker) {
		gameBoardState[column][row] = marker;	//Update the game board in memory with the marker requested.
		
		updateWinner();	//Check to see if anyone wins after each move.
		
		lastMoveColumn = column;	//Store the column for the last move.  This is used for undoing moves.
		lastMoveRow = row;			//Store the row for the last move.  This is used for undoing moves.
	}
	
	/**
	 * Returns the marker in the game board square.  Should always return
	 * either 'X', 'O', or 'N'.
	 * @param column	The column to check.
	 * @param row	The row to check.
	 * @return	Returns the marker in the game board space requested.
	 */
	public char getGameSpaceState(int column, int row) {
		return gameBoardState[column][row];
	}
	
	/**
	 * This method will undo the last move.
	 * @return	Returns true if there was something to undo, else returns
	 * 			false.
	 */
	public boolean undoLastMove() {
		if (getGameSpaceState(lastMoveColumn, lastMoveRow) != 'N') {	//If there is a move to undo
			updateGameSpace(lastMoveColumn, lastMoveRow, 'N');				//Then clear the last move made
			return(true);													//And return true to indicate that the undo was successful.
		}
		else															//There is no move to undo
			return(false);													//So return false to indicate that no move was undone.
	}
	
	
	/**
	 * @param ticTacToeBoardBitmap	Sets the main tic tac toe board image to this.
	 */
	public void setBoardImage(Bitmap ticTacToeBoardBitmap) {
		this.ticTacToeBoardBitmap = ticTacToeBoardBitmap;	//Set the tic tac toe board image.
	}
	
	/**
	 * @param xImage	Sets the X image marker to this.
	 */
	public void setXImage(Bitmap xImage) {
		this.xImage = xImage;	//Set the X image marker.
	}
	
	/**
	 * @param oImage	Sets the O image marker to this.
	 */
	public void setOImage(Bitmap oImage) {
		this.oImage = oImage;	//Set the O image marker.
	}
	
	/**
	 * @param XWinRowImage	Sets the image for the line across a row of X's to this.
	 * @param XWinColumnImage	Sets the image for the line down a column of X's to this.
	 * @param XWinDiag0Image	Sets the image for the line from top-left to bottom-right of X's to this.
	 * @param XWinDiag1Image	Sets the image for the line from top-right to bottom-left of X's to this.
	 */
	public void setXWinImages(Bitmap xWinRowImage, Bitmap xWinColumnImage, Bitmap xWinDiag0Image, Bitmap xWinDiag1Image) {
		this.xWinRowImage = xWinRowImage;		//Set the image for the line across a row of X's.
		this.xWinColumnImage = xWinColumnImage;	//Set the image for the line down a column of X's.
		this.xWinDiag0Image = xWinDiag0Image;	//Set the image for the line from top-left to bottom-right of X's.
		this.xWinDiag1Image = xWinDiag1Image;	//Set the image for the line from top-right to bottom-left of X's.
	}
	
	/**
	 * @param OWinRowImage	Sets the image for the line across a row of O's to this.
	 * @param OWinColumnImage	Sets the image for the line down a column of O's to this.
	 * @param OWinDiag0Image	Sets the image for the line from top-left to bottom-right of O's to this.
	 * @param OWinDiag1Image	Sets the image for the line from top-right to bottom-left of O's to this.
	 */
	public void setOWinImages(Bitmap oWinRowImage, Bitmap oWinColumnImage, Bitmap oWinDiag0Image, Bitmap oWinDiag1Image) {
		this.oWinRowImage = oWinRowImage;		//Set the image for the line across a row of O's.
		this.oWinColumnImage = oWinColumnImage;	//Set the image for the line down a column of O's.
		this.oWinDiag0Image = oWinDiag0Image;	//Set the image for the line from top-left to bottom-right of O's.
		this.oWinDiag1Image = oWinDiag1Image;	//Set the image for the line from top-right to bottom-left of O's.
	}
	
	/**
	 * Returns the tic tac toe board bitmap with the current markers.
	 */
	public Bitmap getBoardImage() {
		Bitmap boardBitmap = ticTacToeBoardBitmap;	//Loads the bare tic tac toe board.
		boardBitmap = drawMarkers(boardBitmap);	//Draws the X and O markers on the board.
		boardBitmap = drawWinnerLine(boardBitmap);	//Draws the winning line through the three winning marks.
		
		return(boardBitmap);	//Return the updated board image.
	}
	
	/**
	 * Draws the X and O markers on the board.
	 * @param baseBitmap	Base bitmap to draw the X's and O's over.
	 * @return		Returns combined bitmap.
	 */
	private Bitmap drawMarkers(Bitmap baseBitmap) {
	    Bitmap bmOverlay = Bitmap.createBitmap(baseBitmap.getWidth(), baseBitmap.getHeight(),  baseBitmap.getConfig());	//Create a bitmap with the same dimensions as the base bitmap.
	    Canvas canvas = new Canvas(bmOverlay);	//Create a canvas to draw on the bmOverlay bitmap.    
	    canvas.drawBitmap(baseBitmap, 0, 0, null);	//Add the base bitmap to the canvas.
    
		for (int row = 0; row < boardSize; row++) {
	    	for (int column = 0; column < boardSize; column++) {
	    	    if (gameBoardState[column][row] == 'X')
	    	    	canvas.drawBitmap(xImage, markerColumnCoordinate[column], markerRowCoordinate[row], null);	//Draw the X marker at the correct locations.
	    	    else if (gameBoardState[column][row] == 'O')
	    	    	canvas.drawBitmap(oImage, markerColumnCoordinate[column], markerRowCoordinate[row], null);	//Draw the O marker at the correct locations.
	    	}
	    }
		
	    return bmOverlay;	//Returns the completed bitmap.
	}
	
	/**
	 * Draws the winning line through the three winning marks.
	 * @param baseBitmap	Base bitmap to draw the line through the three winning marks.
	 * @return		Returns combined bitmap.
	 */
	private Bitmap drawWinnerLine(Bitmap baseBitmap) {
		Bitmap bmOverlay = Bitmap.createBitmap(baseBitmap.getWidth(), baseBitmap.getHeight(),  baseBitmap.getConfig());	//Create a bitmap with the same dimensions as the base bitmap.
	    Canvas canvas = new Canvas(bmOverlay);		//Create a canvas to draw on the bmOverlay bitmap.    
	    canvas.drawBitmap(baseBitmap, 0, 0, null);	//Add the base bitmap to the canvas.
		
		
	    Bitmap winningLineImage;			//Holds the winning line image to display.
		Point lineCoordinate = new Point();	//Holds the x and y coordinates, in pixels, of where to draw the line.
		
		winningLineImage = drawWinnerLineGetLineImage();	//Gets the appropriate image needed to display for the winning markers.
		lineCoordinate = drawWinnerLineGetCoordinates();	//Gets the x and y coordinates, in pixels, of where to draw the image for the line showing the winning move.
	    
	    //Draw the image.
	    if (winningLineImage != null) {	//There was a winner.
	    	canvas.drawBitmap(winningLineImage, lineCoordinate.x, lineCoordinate.y, null);	//So draw line for winner.
	    }
	
	    return bmOverlay;	//Returns the completed bitmap.
	}
	
	/**
	 * @return Returns the ID appropriate image needed to display for the winning markers. 
	 */
	private Bitmap drawWinnerLineGetLineImage() {
		Bitmap returnWinLineImage = null;
		
	    //Get the correct image to draw.
	    if (win.marker == 'X') {	//X was the winner
	    	if (win.type == "ROW")			//X wins across a row.
	    		returnWinLineImage = xWinRowImage;	//So return the row image.
	    	else if (win.type == "COLUMN")	//X wins down a column.
	    		returnWinLineImage = xWinColumnImage;	//So return the column image.
	    	else if (win.type == "DIAG0")	//X wins from top-left to bottom-right.
	    		returnWinLineImage = xWinDiag0Image;	//So return the diag0 image.
	    	else if (win.type == "DIAG1")	//X wins from top-right to bottom-left.
	    		returnWinLineImage = xWinDiag1Image;	//So return the diag1 image.
	    }
	    else if (win.marker == 'O') {	//O was the winner
	    	if (win.type == "ROW")			//O wins across a row.
	    		returnWinLineImage = oWinRowImage;	//So return the row image.
	    	else if (win.type == "COLUMN")	//O wins down a column.
	    		returnWinLineImage = oWinColumnImage;	//So return the column image.
	    	else if (win.type == "DIAG0")	//O wins from top-left to bottom-right.
	    		returnWinLineImage = oWinDiag0Image;	//So return the diag0 image.
	    	else if (win.type == "DIAG1")	//O wins from top-right to bottom-left.
	    		returnWinLineImage = oWinDiag1Image;	//So return the diag1 image.
	    }

	    return(returnWinLineImage);	//Return the image.
	}
	
	/**
	 * @return Returns the x and y coordinates, in pixels, of where to draw the image for the line showing the winning move.
	 */
	private Point drawWinnerLineGetCoordinates() {
		Point returnedPoint = new Point();	//Point to return.
		
	    //Get the x and y coordinates for the image.
	    if (win.marker != 'N') {	//There was a winner.
	    	if (win.type == "ROW") {	//Win is across a row.
	    		returnedPoint.x = markerColumnCoordinate[0];	//X coordinate is same as left marker.
	    		if (win.position == 0)				//Win position is top row.
	    			returnedPoint.y = markerRowCoordinate[0];		//Y coordinate is same as top markers.
	    		else if (win.position == 1)			//Win position is middle row.
	    			returnedPoint.y = markerRowCoordinate[1];		//Y coordinate is same as middle markers.
	    		else								//Win position is bottom row.
	    			returnedPoint.y = markerRowCoordinate[2];		//Y coordinate is same as bottom markers.
	    	}
	    	else if (win.type == "COLUMN") {	//Win is down a column.
	    		returnedPoint.y = markerRowCoordinate[0];		//Y coordinate is same as top marker.
	    		if (win.position == 0)				//Win position is left column.
	    			returnedPoint.x = markerColumnCoordinate[0];	//X coordinate is same as left markers.
	    		else if (win.position == 1)			//Win position is middle column.
	    			returnedPoint.x = markerColumnCoordinate[1];	//X coordinate is same as middle markers.
	    		else								//Win position is right column.
	    			returnedPoint.x = markerColumnCoordinate[2];	//X coordinate is same as right markers.
	    	}
	    	else if (win.type == "DIAG0") {	//Win is from top-left to bottom-right.
	    		returnedPoint.x = markerColumnCoordinate[0];	//X coordinate is same as left marker.
	    		returnedPoint.y = markerRowCoordinate[0];		//Y coordinate is same as top marker.
	    	}
	    	else if (win.type == "DIAG1") {	//O wins from top-right to bottom-left.
	    		returnedPoint.x = markerColumnCoordinate[0];	//X coordinate is same as left marker.  Note:  The actually image is almost as big as the entire board.
	    		returnedPoint.y = markerRowCoordinate[0];		//Y coordinate is same as top marker.
	    	}
	    }
	    
	    return(returnedPoint);	//Return the point.
	}
	
	/**
	 * @return	Returns 'X' if X wins, returns 'O' is O wins, returns 'T' if
	 * the game is a tie, or returns 'N' if there is no winner.
	 */
	public char getWinner() {
		return (win.marker);
	}
	
	/**
	 * Checks for a winner, and updates the win class with the winning
	 * information.  Sets win.marker to 'X' if X wins, 'O' is O wins, 'T' if
	 * the game is a tie, or 'N' if there is no winner.
	 */
	private void updateWinner() {
		
		if (getNumberOfOpenSquares() == 0)
			win.marker = 'T';	//Tie
		
		checkForWinnerColumns();
		checkForWinnerRows();
		checkForWinnerDiagonal0();
		checkForWinnerDiagonal1();
	}
		
	/**
	 * Checks for a winner in the vertical columns.  If there is a winner, then
	 * this method updates the win class with the winning information.
	 * Sets win.marker to 'X' if X wins, or 'O' if O wins.
	 * Sets win.type to "COLUMN" if there is a winner in one of the columns.
	 * Sets win.position to the corresponding win column.  Column 0 is the left
	 * column, 1 the middle column, and 2 the right column.
	 */
	private void checkForWinnerColumns() {
		int column = 0;
		while (column < boardSize) {
			if (gameBoardState[column][0] != 'N')
				if (gameBoardState[column][0] == gameBoardState[column][1])
					if (gameBoardState[column][1] == gameBoardState[column][2]) {
						win.marker = gameBoardState[column][0];
						win.type = "COLUMN";
						win.position = column;
					}
			column++;
		}
	}

	/**
	 * Checks for a winner in the horizontal rows.  If there is a winner, then
	 * this method updates the win class with the winning information.
	 * Sets win.marker to 'X' if X wins, or 'O' if O wins.
	 * Sets win.type to "ROW" if there is a winner in one of the rows.
	 * Sets win.position to the corresponding win row.  Row 0 is the top row, 1
	 * the middle row, and 2 the bottom row.
	 */
	private void checkForWinnerRows() {
		int row = 0;
		while (row < boardSize) {
			if (gameBoardState[0][row] != 'N')
				if (gameBoardState[0][row] == gameBoardState[1][row])
					if (gameBoardState[1][row] == gameBoardState[2][row]) {
						win.marker = gameBoardState[0][row];
						win.type = "ROW";
						win.position = row;
					}
			row++;
		}
	}
	
	/**
	 * Checks for a winner in the top-left to bottom-right diagonal.  If there
	 * is a winner, then this method updates the win class with the winning
	 * information.
	 * Sets win.marker to 'X' if X wins, or 'O' if O wins.
	 * Sets win.type to "DIAG0" if there is a winner in the diagonal.
	 */
	private void checkForWinnerDiagonal0() {
		
		if (gameBoardState[0][0] != 'N')
			if (gameBoardState[0][0] == gameBoardState[1][1])
				if (gameBoardState[1][1] == gameBoardState[2][2]) {
					win.marker = gameBoardState[0][0];
					win.type = "DIAG0";
				}
		
	}
	
	/**
	 * Checks for a winner in the top-right to bottom-left diagonal.  If there
	 * is a winner, then this method updates the win class with the winning
	 * information.
	 * Sets win.marker to 'X' if X wins, or 'O' if O wins.
	 * Sets win.type to "DIAG1" if there is a winner in the diagonal.
	 */
	private void checkForWinnerDiagonal1() {
		
		if (gameBoardState[2][0] != 'N')
			if (gameBoardState[2][0] == gameBoardState[1][1])
				if (gameBoardState[1][1] == gameBoardState[0][2]) {
					win.marker = gameBoardState[2][0];
					win.type = "DIAG1";
				}
		
	}
	
	/**
	 * @return	Returns the number of square that do not have an 'X' or 'O' in them.
	 */
	public int getNumberOfOpenSquares() {
		int count = 0;
		
		for (int row = 0; row < boardSize; row++)
	    	for (int column = 0; column < boardSize; column++)
	    		if (gameBoardState[column][row] == 'N')
	    			count++;
	    	
	    return count;
	}
	
}
