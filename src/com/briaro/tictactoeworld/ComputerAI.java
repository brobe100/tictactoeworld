package com.briaro.tictactoeworld;

import java.util.Random;


public class ComputerAI {
	
	Random random = new Random();
	
	int selectedColumn;
	int selectedRow;
	
	class MoveScore {
		int major;		//Contains the major score.  0 if a sure lose, 1 if a tie, and 2 if a win.
		double minor;	//Contains the minor score.  If the GameScore.major is the same between two possible moves, then use the GameScore.minor.
	
		MoveScore(int major, double minor) {
			this.major = major;
			this.minor = minor;
		}
	}
	
	/**
	 * Generate a computer move for a tic-tac-toe board.
	 * @param currentGameboard	The game board to generate the computers move
	 * from.
	 * @param level	The difficult level for the AI.
	 * @param aiMarker	The marker used by the AI, either X or O.
	 */
	public void generateComputerMove(Gameboard currentGameboard, int level, char aiMarker) {
		MoveScore[][] moveScoreArray;	//Used to contain the score for each move.  A move is placing a O marker in the corresponding space.
		
		moveScoreArray = generateComputerMoveScoreArray(currentGameboard, aiMarker, level);	//Generate the score array.
		
		randomArray(moveScoreArray);	//Randomize the score a little bit.
		
		//Find the maximum score of all the moves, and set the selected column and row to that move.
		int maximumMajorScore = moveScoreArray[0][0].major;	//Initialize the major score maximum.
		double maximumMinorScore = moveScoreArray[0][0].minor;	//Initialize the minor score maximum.
		selectedColumn = 0;	//Initialize selected column to first space.
		selectedRow = 0;	//Initialize selected row to first space.
		
		for (int checkColumn = 0; checkColumn < 3; checkColumn++)
			for (int checkRow = 0; checkRow < 3; checkRow++) {
				if (moveScoreArray[checkColumn][checkRow].major > maximumMajorScore) {
					maximumMajorScore = moveScoreArray[checkColumn][checkRow].major;	//Update major score maximum.
					maximumMinorScore = moveScoreArray[checkColumn][checkRow].minor;	//Update minor score maximum.
					selectedColumn = checkColumn;	//Set selected column to this column.
					selectedRow = checkRow;	//Set selected row to this row.
				}
				else if (moveScoreArray[checkColumn][checkRow].major == maximumMajorScore) {	//Another major score is found that is the same, so
					if (moveScoreArray[checkColumn][checkRow].minor > maximumMinorScore) {		//update the maximum minor score if the new minor score is higher.
						maximumMinorScore = moveScoreArray[checkColumn][checkRow].minor;	//This updates the maximum minor score if the new minor score is higher.
						selectedColumn = checkColumn;	//Set selected column to this column.
						selectedRow = checkRow;	//Set selected row to this row.
					}
				} 
			}
	}
	
	/**
	 * Calculates a score for placing a marker on a tic-tac-toe board.
	 * @param currentGameboard	The game board to generate the score on.
	 * @param marker	The marker to generate the score for.
	 * @param level	The difficult level for the AI.
	 * @return	Returns a score for the game board with the marker in
	 * every open space.  The higher the score, the better the move for the AI.
	 */
	private MoveScore generateComputerMoveScore(Gameboard currentGameboard, char marker, int level) {
		MoveScore[][] moveScoreArray;
		
		moveScoreArray = generateComputerMoveScoreArray(currentGameboard, marker, level);	//Generate the score array.
		
		return(getHighestScoreFromArray(moveScoreArray));	//Get the score from the score array.
	}
	
	/**
	 * Calculates a array of scores for placing a marker on a tic-tac-toe
	 * board.
	 * @param currentGameboard	The game board to generate the score on.
	 * @param marker	The marker to generate the score for.
	 * @param level	The difficult level for the AI.
	 * @return	Returns an array of scores for the game board with the marker
	 * in every open space.  The higher the score, the better the move for the
	 * AI.
	 */
	private MoveScore[][] generateComputerMoveScoreArray(Gameboard currentGameboard, char marker, int level) {
		MoveScore[][] moveScoreArray = new MoveScore[3][3];
		char opponentMarker;	//Marker used by the opponent.
		
		//Set the opponent's markers.
		if (marker == 'X')	//Set the opponent's marker to the opposite.
			opponentMarker = 'O';
		else
			opponentMarker = 'X';
		
		for (int testColumn = 0; testColumn < 3; testColumn++)							//Check each column
			for (int testRow = 0; testRow < 3; testRow++) {								//and each row
				moveScoreArray[testColumn][testRow] = new MoveScore(-1, -1);	//Initialize the score to -1, which indicates that the move cannot be made.
				if (currentGameboard.getGameSpaceState(testColumn, testRow) == 'N') {	//for a open space.
					Gameboard testGameboard = new Gameboard(currentGameboard);	//If space is open, make a fresh copy of the game board to mark up.
					testGameboard.updateGameSpace(testColumn, testRow, marker);	//Place the marker in the open space.
					if (testGameboard.getWinner() == marker) {	//If the move makes the current player win
						moveScoreArray[testColumn][testRow].major = 2;		//then it gets a score of 2.
						moveScoreArray[testColumn][testRow].minor = 2;
					}
					else if (testGameboard.getWinner() == opponentMarker) {	//If the move makes the opponent win
						moveScoreArray[testColumn][testRow].major = 0;		//then it gets a score of 0.
						moveScoreArray[testColumn][testRow].minor = 0;
					}
					else if (testGameboard.getWinner() == 'T') {	//If the move makes a tied game
						moveScoreArray[testColumn][testRow].major = 1;		//then it gets a score of 1.
						moveScoreArray[testColumn][testRow].minor = 1;
					}
					else {
						if (level <= 1) {
							moveScoreArray[testColumn][testRow].major = 1;	//Last AI level, so just give it a 1.
							moveScoreArray[testColumn][testRow].minor = 1;
						}
						else {
							MoveScore scoreForOpponentsNextMove;
							scoreForOpponentsNextMove = generateComputerMoveScore(testGameboard, opponentMarker, (level - 1));	//Else, check a level deeper.
							moveScoreArray[testColumn][testRow].major = 2 - scoreForOpponentsNextMove.major;	//Since a score was generated for the opponents move, subtract it from 2.
							moveScoreArray[testColumn][testRow].minor = 2 - scoreForOpponentsNextMove.minor;	//Since a score was generated for the opponents move, subtract it from 2.
						}
					}
				}
			}
		
		return(moveScoreArray);	//Get the entire score array.
	}
	
	/**
	 * Gets the appropriate score from the score array.  Returns the highest
	 * major score, and the average of the minor score.
	 * @param moveScoreArray	The array holding the score for each move.
	 * @return	Returns the highest major score, and the average of the minor
	 * score.
	 */
	private MoveScore getHighestScoreFromArray(MoveScore[][] moveScoreArray) {
		MoveScore returnedMoveScore = new MoveScore(0,0);	//Create the move score to return. 
		returnedMoveScore.major = moveScoreArray[0][0].major;	//Initialize the returned move score.
		returnedMoveScore.minor = 0;	//Initialize the returned minor score to 0.
		
		//Get the maximum major score.
		for (int checkColumn = 0; checkColumn < 3; checkColumn++)
			for (int checkRow = 0; checkRow < 3; checkRow++)
				if (moveScoreArray[checkColumn][checkRow].major > returnedMoveScore.major)
					returnedMoveScore.major = moveScoreArray[checkColumn][checkRow].major;	//Update the returned major score.
		
		//Get the average minimum score.
		double minorScoresCount = 0;	//Used to count the number of minor scores that are greater or equal to 0.
		for (int checkColumn = 0; checkColumn < 3; checkColumn++)
			for (int checkRow = 0; checkRow < 3; checkRow++)
				if (moveScoreArray[checkColumn][checkRow].minor >= 0) {						//Found a minor score greater than or equal to 0,
					returnedMoveScore.minor += moveScoreArray[checkColumn][checkRow].minor;	//so add it to the minor score total
					minorScoresCount++;														//and increment the number of minor scores found.
				}
		if (minorScoresCount > 0)													//Some valid minor scores were found,
			returnedMoveScore.minor = returnedMoveScore.minor / minorScoresCount;	//so get the average
		else																		//else
			returnedMoveScore.minor = 0;											//return a 0.
		
		return (returnedMoveScore);	//Return the composite score.
	}
	
	/**
	 * Slightly randomizes the scores in the score array.
	 * @param moveScoreArray	Array holding an array of scores.
	 */
	private void randomArray(MoveScore[][] moveScoreArray) {
		for (int column = 0; column < 3; column++)
			for (int row = 0; row < 3; row++)
				if (moveScoreArray[column][row].minor > 0)
					moveScoreArray[column][row].minor *= (1 + (random.nextDouble() * 0.1));
	}
	
	/**
	 * @return	Returns the column for the best move.
	 */
	public int getComputerMoveColumn() {
		return selectedColumn;
	}
		
	/**
	 * @return	Returns the row for the best move.
	 */
	public int getComputerMoveRow() {
		return selectedRow;
	}
	
}
