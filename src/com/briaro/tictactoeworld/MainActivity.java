package com.briaro.tictactoeworld;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/WTTWmessy.ttf");	//Load the font.
		TextView tv;	//Used to update each TextView font.
		Button button;	//Used to update each Button font.
		String versionName = "Error getting version name.";	//String to hold version name.
		
		//Update version name label
		try {
			versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tv = (TextView) findViewById(R.id.version_name_label);	//Get the version name label on main menu.
		tv.setText(versionName);	//Update the version name label.
		tv.setTypeface(tf);			//Update version name font.
			
		//Update other fonts.
		tv = (TextView) findViewById(R.id.main_menu_company_name);	//Update company name on main menu.
		tv.setTypeface(tf);
		tv = (TextView) findViewById(R.id.main_menu_app_title);	//Update application title on main menu.
		tv.setTypeface(tf);
		button = (Button) findViewById(R.id.single_player_button);	//Update single player button on main menu.
		button.setTypeface(tf);
		button = (Button) findViewById(R.id.player_vs_player_button);	//Update player vs player button on main menu.
		button.setTypeface(tf);
		button = (Button) findViewById(R.id.computer_vs_computer_button);	//Update computer vs computer button on main menu.
		button.setTypeface(tf);
		
	}

	public void singlePlayerButtonClicked(View view) {
	    Intent intent = new Intent(this, GameActivity.class);
	    Bundle bundle = new Bundle();
	    bundle.putString(GameActivity.FIRST_PLAYER, GameActivity.FIRST_PLAYER_RANDOM);	//Set the first player to random.
	    bundle.putString(GameActivity.X_PLAYER, GameActivity.PLAYER_USER);		//Set X player to user.
	    bundle.putString(GameActivity.O_PLAYER, GameActivity.PLAYER_COMPUTER);	//Set O player to computer
	    bundle.putInt(GameActivity.O_COMPUTER_LEVEL, 0);	//Set O computer level.  Passing a zero means to automatically set the level.
	    intent.putExtras(bundle);
	    startActivity(intent);
	}
		
	public void playerVsPlayerButtonClicked(View view) {
	    Intent intent = new Intent(this, GameActivity.class);
	    Bundle bundle = new Bundle();
	    bundle.putString(GameActivity.FIRST_PLAYER, GameActivity.FIRST_PLAYER_RANDOM);	//Set the first player to random.
	    bundle.putString(GameActivity.X_PLAYER, GameActivity.PLAYER_USER);	//Set x player to user.
	    bundle.putString(GameActivity.O_PLAYER, GameActivity.PLAYER_USER);	//Set o player to user
	    intent.putExtras(bundle);
	    startActivity(intent);
	}
		
	public void customGameButtonClicked(View view) {
	    Intent intent = new Intent(this, CustomGameActivity.class);
	    startActivity(intent);
	}
	
}

