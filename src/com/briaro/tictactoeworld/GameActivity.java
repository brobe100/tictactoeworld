package com.briaro.tictactoeworld;

import java.util.Random;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class GameActivity extends Activity {
	/**
	 * Used to set the state of the game.
	 * @author Brian Roberts
	 *
	 */
	private enum GameState {NEW_GAME, XTURN, OTURN, WINNER, NEXT_GAME_CONFIRM}
	
	public final static String FIRST_PLAYER = "com.briaro.tictactoeworld.FIRST_PLAYER";	//Key to tell who is the first player (X, O, or Random).
	public final static String FIRST_PLAYER_RANDOM = "Random";	//String to indicate that the first player is random.
	public final static String FIRST_PLAYER_X = "X";	//String to indicate that the first player is X.
	public final static String FIRST_PLAYER_O = "O";	//String to indicate that the first player is O.
	public final static String X_PLAYER = "com.briaro.tictactoeworld.X_PLAYER";	//Key to tell who is the x player (user or computer).
	public final static String O_PLAYER = "com.briaro.tictactoeworld.O_PLAYER";	//Key to tell who is the o player (user or computer).
	public final static String PLAYER_USER = "User";			//Used to indicated that the player is the user.
	public final static String PLAYER_COMPUTER = "Computer";	//Used to indicated that the player is the computer.
	public final static String X_COMPUTER_LEVEL = "com.briaro.tictactoeworld.X_COMPUTER_LEVEL";	//Key to set the X computer level.  And integer is passed with this key, and a zero that is passed means to automatically select the computer level.
	public final static String O_COMPUTER_LEVEL = "com.briaro.tictactoeworld.O_COMPUTER_LEVEL";	//Key to set the X computer level.  And integer is passed with this key, and a zero that is passed means to automatically select the computer level.
	
	private boolean gameActivityPaused = false;	//Used to indicate the game is paused. 
	private GameState gameState = GameState.NEW_GAME;	//Start off at new game state.
	private GameState lastGameState = GameState.NEW_GAME;	//Used to determine when to update the UI.
	private Gameboard currentGameboard;	//Create the tic tac toe game board.
	private ComputerAI computerAI;	//Create the computer AI.
	private UserInterface userInterface;	//Used to interact with the user clicking the game board.
	private int numOfXWins = 0, numOfOWins = 0, numOfTied = 0;	//Used to count number of wins and tied games.
	private int computerLevelX = 1, computerLevelO = 1;	//The computer level for X and O.
	private boolean xAutomaticLevelSelection, oAutomaticLevelSelection;	//Set to true if the level should be automatically incremented or decremented.
	private String firstPlayer;	//Sets who the first player will be.  Use FIRST_PLAYER_RANDOM, FIRST_PLAYER_X, or FIRST_PLAYER_O constants.
	private String xPlayer = PLAYER_USER, oPlayer = PLAYER_COMPUTER;	//Sets if the X and O are controlled by the user or player.  Valid values are PLAYER_USER and PLAYER_COMPUTER.
	private ImageView ticTacToeBoard;	//Tic tac toe board image.
	private Button undoButton;	//Button to undo the last move.
	private TextView xTurnLabel;	//Label to indicate if it is X's turn.
	private TextView oTurnLabel;	//Label to indicate if it is O's turn.
	private TextView xWinsLabel;	//Label to indicate number of X wins.
	private TextView oWinsLabel;	//Label to indicate number of O wins.
	private TextView tiedGamesLabel;	//Label to indicate number tied games.
	private TextView xLevelLabel;	//Label to indicate the computer level for X.
	private TextView oLevelLabel;	//Label to indicate the computer level for O.
	private TextView endGamePromptWinnerLabel;	//Label to indicate who the winner was in the end game prompt.
	private TextView endGamePromptNextXLevelLabel;	//Label to indicate the next X computer level in the end game prompt.
	private TextView endGamePromptNextOLevelLabel;	//Label to indicate the next O computer level in the end game prompt.
	private Button endGamePromptButton;	//Button to dismiss the next game prompt and continue.
	private Typeface tf;	//Font used for all text.
	private View endGamePromptContent;	//Content message to show in the end game prompt.
	private Dialog endGamePromptDialog;	//Dialog to show at the end of a game.
	private boolean waitingForUserToClickNextGamePrompt = false;	//Used to indicate if the application is waiting for the user to click the next game prompt.
	private float turnLabelDefaultTextSize;	//Used to store the turn label default text size.  This will be set in onCreate().
	private float turnLabelLargeTextSize;	//Used to store the turn label large text size.  This will be set in onCreate().
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_game);
		//Set up Image and TextView references.
		ticTacToeBoard = (ImageView) findViewById(R.id.tic_tic_toe_board_view);	//Tic tac toe board image in the activity.
		undoButton = (Button) findViewById(R.id.undo_button);					//Button to undo the last move.
		xTurnLabel = (TextView) findViewById(R.id.x_turn);						//Label to indicate if it is X's turn.
		oTurnLabel = (TextView) findViewById(R.id.o_turn);						//Label to indicate if it is O's turn.
		xWinsLabel = (TextView) findViewById(R.id.x_wins);						//Label to indicate number of X wins.
		oWinsLabel = (TextView) findViewById(R.id.o_wins);						//Label to indicate number of O wins.
		tiedGamesLabel = (TextView) findViewById(R.id.tied_games);				//Label to indicate number tied games.
		xLevelLabel = (TextView) findViewById(R.id.x_level);					//Label to indicate the computer level for X.
		oLevelLabel = (TextView) findViewById(R.id.o_level);					//Label to indicate the computer level for O.
		
		turnLabelDefaultTextSize = xTurnLabel.getTextSize();		//Get the turn labels default line height.
		turnLabelLargeTextSize = turnLabelDefaultTextSize * 1.5f;	//Set the turn label large line height to 1.5x the default line height.
		
		updateFonts();	//Updates the fonts for the TextView labels and Main Menu button.
		
		createGameBoard();	//Create the tic tac toe game board.
				
		computerAI = new ComputerAI();			//Create the computer AI.
		userInterface = new UserInterface();	//Used to interact with the user clicking the game board.
		
		//Get the intent with the information that was passed.
		Intent intent = getIntent();
		
		//Get the first player from the intent
		firstPlayer = intent.getStringExtra(FIRST_PLAYER);
		if (firstPlayer == null)	//If no first player was passed
			firstPlayer = FIRST_PLAYER_RANDOM;	//This select random first player.
				
		//Get the information for setting if X and O are played by the user or computer.
		xPlayer = intent.getStringExtra(X_PLAYER);	//See if the X player is the user or computer.
		oPlayer = intent.getStringExtra(O_PLAYER);	//See if the O player is the user or computer.
		
		//Show the Undo button if it is a player vs player game, else hide the undo button.
		if (xPlayer.equals(PLAYER_USER) && oPlayer.equals(PLAYER_USER))	//If player vs player game
			undoButton.setVisibility(View.VISIBLE);							//Then show the undo button,
		else															//Else
			undoButton.setVisibility(View.INVISIBLE);						//Hide the undo button.
		
		//Get the X computer level.
		int passedComputerLevel = intent.getIntExtra(X_COMPUTER_LEVEL, 0);	//Get the X computer level passed.
		if (passedComputerLevel == 0) {			//Automatic mode selected.
			xAutomaticLevelSelection = true;		//Set automatic mode.
			computerLevelX = 1;						//And start off at level 1.
		}
		else {									//Automatic mode not selected.
			xAutomaticLevelSelection = false;		//Disable automatic mode.
			computerLevelX = passedComputerLevel;	//And start off at level passed.
		}
		
		//Get the O computer level.
		passedComputerLevel = intent.getIntExtra(O_COMPUTER_LEVEL, 0);	//Get the O computer level passed.
		if (passedComputerLevel == 0) {			//Automatic mode selected.
			oAutomaticLevelSelection = true;		//Set automatic mode.
			computerLevelO = 1;						//And start off at level 1.
		}
		else {									//Automatic mode not selected.
			oAutomaticLevelSelection = false;		//Disable automatic mode.
			computerLevelO = passedComputerLevel;	//And start off at level passed.
		}
	}

	
	/**
	 * Creates the tic tac toe board instance, and sets up all the images used for the board.
	 */
	private void createGameBoard() {
		Bitmap[] image = new Bitmap[4];	//Used to set up images.
		
		currentGameboard = new Gameboard(this);	//Create the tic tac toe game board instance.
		image[0] = BitmapFactory.decodeResource(getResources(), R.drawable.tic_tak_toe_board);	//Loads the bare tic tac toe board.
		currentGameboard.setBoardImage(image[0]);	//Set the base board image.
		image[0] = BitmapFactory.decodeResource(getResources(), R.drawable.x);	//Loads the X marker.
		currentGameboard.setXImage(image[0]);	//Set the X image.
		image[0] = BitmapFactory.decodeResource(getResources(), R.drawable.o);	//Loads the O marker.
	    currentGameboard.setOImage(image[0]);	//Set the O image.
	    
	    //Load X winning images.
	    image[0] = BitmapFactory.decodeResource(getResources(), R.drawable.x_win_row);		//Loads the image for the line across a row of X's.
	    image[1] = BitmapFactory.decodeResource(getResources(), R.drawable.x_win_column);	//Loads the image for the line down a column of X's.
	    image[2] = BitmapFactory.decodeResource(getResources(), R.drawable.x_win_diag0);	//Loads the image for the line from top-left to bottom-right of X's.
	    image[3] = BitmapFactory.decodeResource(getResources(), R.drawable.x_win_diag1);	//Loads the line from top-right to bottom-left of X's.
	    currentGameboard.setXWinImages(image[0], image[1], image[2], image[3]);				//Set the X win images.
	    
	    //Load O winning images.
	    image[0] = BitmapFactory.decodeResource(getResources(), R.drawable.o_win_row);		//Loads the image for the line across a row of O's.
	    image[1] = BitmapFactory.decodeResource(getResources(), R.drawable.o_win_column);	//Loads the image for the line down a column of O's.
	    image[2] = BitmapFactory.decodeResource(getResources(), R.drawable.o_win_diag0);	//Loads the image for the line from top-left to bottom-right of O's.
	    image[3] = BitmapFactory.decodeResource(getResources(), R.drawable.o_win_diag1);	//Loads the line from top-right to bottom-left of O's.
	    currentGameboard.setOWinImages(image[0], image[1], image[2], image[3]);				//Set the O win images.
	}
	
	@Override
	public void onResume() {
		super.onResume();
		gameActivityPaused = false;	//Used to indicate that the game activity is no longer paused.
		Thread updateGameStateThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(gameActivityPaused == false) {
					gameStateMachine();	//Continually update game state machine.
					try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		});
		updateGameStateThread.start();
	}

	@Override
	public void onPause() {
		super.onPause();
		gameActivityPaused = true;	//Used to indicate that the activity is being paused, so the game state thread should exit.
	}
	
	/**
	 * Called when user touches the main menu button.  Returns the user to the
	 * main menu.
	 * @param view	The view that was clicked.
	 */
	public void mainMenuButtonClicked(View view) {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	/**
	 * Called when user touches the undo button.  This will undo the last move.
	 * Note that the undo button should only be shown for player vs player
	 * games.
	 * @param view	The view that was clicked.
	 */
	public void undoButtonClicked(View view) {
		if ((gameState != GameState.XTURN) && (gameState != GameState.OTURN))	//It is no ones turn
				return;																//So exit this method and do nothing.
		
		if (currentGameboard.undoLastMove() == true) {	//Tell the Gameboard class to undo the last move, and see if it was successful.
			if (gameState == GameState.XTURN)				//It is currently X's turn
				gameState = GameState.OTURN;					//So make it O's turn
			else											//It is currently O's turn
				gameState = GameState.XTURN;					//So make it X's turn
		}
			
		
	}
	
	private int fieldImgXY[] = new int[2];	//Used to indicated the screen coordinates of where the actually tic tac toe board image is located on the screen.
	private float ticTacToeImageScale;	//Used to indicate the scaled image size of the tic tac toe board.

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
	    super.onWindowFocusChanged(hasFocus);

	    // Use onWindowFocusChanged to get the placement of
	    // the image because we have to wait until the image
	    // has actually been placed on the screen before we
	    // get the coordinates. That makes it impossible to
	    // do in onCreate, that would just give us (0, 0).
	    
	    //ticTacToeBoard.getLocationOnScreen(fieldImgXY);	//Get board location on screen.
	    fieldImgXY = getBitmapLocationOnScreen(ticTacToeBoard);	//Get board location on screen.
	    //Log.i("DEBUG", "Tic Tac Toe board image location on screen: " + fieldImgXY[0] + " " + fieldImgXY[1]);
	    
	    ticTacToeImageScale = getBitmapScaleX(ticTacToeBoard);	//Get tic tac toe board image scale.
	    //Log.i("DEBUG", "Tic Tac Toe board image scaler: " + ticTacToeImageScale);
	}
	
	/**
	 * Returns the image location on the screen.
	 * @param img	ImageView to get bitmap location from.
	 * @return	Returns the X and Y coordinates in screen pixels.
	 */
	public static int[] getBitmapLocationOnScreen(ImageView img) {
        int[] fieldImgXY = new int[2];
        float[] values = new float[9];

        Matrix m = img.getImageMatrix();
        m.getValues(values);
        
        img.getLocationOnScreen(fieldImgXY);	//Get ImageView location on screen.

        fieldImgXY[0] += (int) values[Matrix.MTRANS_X];	//Add the matrix translation.
        fieldImgXY[1] += (int) values[Matrix.MTRANS_Y];	//Add the matrix translation.

        return fieldImgXY;
    }
	
	/**
	 * Returns the horizontal scale of the bitmap inside an ImageView.  Use
	 * this instead of ImageView.getScaleX() since getScaleX is only usable
	 * with API 11 and later.
	 * @param img	ImageView to get bitmap scale from.
	 * @return	Returns the horizontal scale of the bitmap inside an ImageView.
	 */
	public static float getBitmapScaleX(ImageView img) {
        float scale;
        float[] values = new float[9];

        Matrix m = img.getImageMatrix();
        m.getValues(values);

        scale = values[Matrix.MSCALE_Y];

        return scale;
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
	        //Log.i("DEBUG", "touch event - down");
	    	
	    	int eventX = (int) event.getX();	//Get x coordinate where user touched on the screen.
	        int eventY = (int) event.getY();	//Get y coordinate where user touched on the screen.
	        //Log.i("DEBUG", "Click location on screen: " + eventX + " " + eventY);

	        int xOnField = (int)((eventX - fieldImgXY[0]) / ticTacToeImageScale);	//Get x coordinate where user touched on the image.
	        int yOnField = (int)((eventY - fieldImgXY[1]) / ticTacToeImageScale);	//Get y coordinate where user touched on the image.
	        //Log.i("DEBUG", "Click location on Tic Tac Toe board image: " + xOnField + " " + yOnField);
	        
	        userInterface.getRowAndColumnOfUserTouch(xOnField, yOnField);	//Update the tic tac toe column and row where the user clicked.
	    
	        updateUI();	//For UI update, so to give the user instant feedback.
	    }
	    return super.onTouchEvent(event);
	}
	
	/**
	 * Called when user clicks the next game button in the prompt after the end
	 * of a game.
	 * @param view	The view that was clicked.
	 */
	public void endGamePromptButtonClicked(View view) {
		endGamePromptDialog.dismiss();	//Close the prompt.
		//waitingForUserToClickNextGamePrompt = false;	//No longer waiting for the user to click the prompt.
	}

	/**
	 * Updates the fonts for the TextView labels.
	 */
	private void updateFonts() {
		tf = Typeface.createFromAsset(getAssets(), "fonts/WTTWmessy.ttf");	//Load the font.
		
		Button mainMenuButton = (Button) findViewById(R.id.main_menu_button);	//Update main menu button.
		mainMenuButton.setTypeface(tf);	//Update font for main menu button.
		undoButton.setTypeface(tf);	//Update font for undo button.
		
		xTurnLabel.setTypeface(tf);						//Label to indicate if it is X's turn.
		oTurnLabel.setTypeface(tf);						//Label to indicate if it is O's turn.
		xWinsLabel.setTypeface(tf);						//Label to indicate number of X wins.
		oWinsLabel.setTypeface(tf);						//Label to indicate number of O wins.
		tiedGamesLabel.setTypeface(tf);					//Label to indicate number tied games.
		xLevelLabel.setTypeface(tf);					//Label to indicate the computer level for X.
		oLevelLabel.setTypeface(tf);					//Label to indicate the computer level for O.
	}
	
	/**
	 * Updates the game state machine.
	 */
	private void gameStateMachine() {
		switch (gameState) {
			case NEW_GAME:					newGameState();				break;	//New game state.
			case XTURN:						xTurnState();				break;	//X's turn state.
			case OTURN:						oTurnState();				break;	//O's turn state.
			case WINNER:					winnerState();				break;	//State when there is a winner.
			case NEXT_GAME_CONFIRM:			nextGameConfirmState();		break;	//State to wait for the user to click the prompt.
		}
		
		//Update UI if needed.
		if (gameState != lastGameState) {	//Game state has changed,
			Thread updateUIThread = new Thread(new Runnable() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							updateUI();	//So create thread to update the UI.
						}
					});
			}});
			updateUIThread.start();
			lastGameState = gameState;	//Update last game state.
		}
	}
	
	/**
	 * Updates the game screen, which includes the turn labels, number of wins labels, the player/AI labels, and the tic tac toe board.
	 */
	private void updateUI() {
		updateTurnLabels();	//Update the turn labels.
		updateWinsLabels();	//Update the wins labels.
		updateAILevelLabels();	//Update the computer AI level labels.
		ticTacToeBoard.setImageBitmap(currentGameboard.getBoardImage());	//Draw the final board.
	}
	
	/**
	 * Called when a new game it started.  Starts the game with X's turn.
	 */
	private void newGameState() {
		Random random = new Random();	//Get a new random number generator.
		
		if (firstPlayer.equals(FIRST_PLAYER_RANDOM))	//If the user selected random for the first player
			if (random.nextInt(2) == 0)						//Then randomly pick a first player.
				gameState = GameState.XTURN;					//X was picked randomly as the first player.
			else
				gameState = GameState.OTURN;					//O was picked randomly as the first player.
		else if (firstPlayer.equals(FIRST_PLAYER_X))	//If the user selected X for the first player
			gameState = GameState.XTURN;					//So set next state as X starting.
		else
			gameState = GameState.OTURN;					//Else set next state as O starting.
	}
	
	/**
	 * Updates the labels used to who turn it is to play. 
	 */
	private void updateTurnLabels() {
		if (gameState == GameState.XTURN) {
		    xTurnLabel.setText("X's turn");	//Indicate that it is X's turn to play.
		    xTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelLargeTextSize);	//Make the label large size.
		    oTurnLabel.setText("O");		//Indicate that it is not O's turn to play.
		    oTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
		}
		else if (gameState == GameState.OTURN) {
		    xTurnLabel.setText("X");		//Indicate that it is not X's turn to play.
		    xTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
		    oTurnLabel.setText("O's turn");	//Indicate that it is O's turn to play.
		    oTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelLargeTextSize);	//Make the label large size.
		}
		else if (gameState == GameState.WINNER) {
			if (currentGameboard.getWinner() == 'X') {
		    	xTurnLabel.setText("X WINS!!!");
		    	xTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelLargeTextSize);	//Make the label large size.
			}
			else if  (currentGameboard.getWinner() == 'O') {
		    	oTurnLabel.setText("O WINS!!!");
		    	oTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelLargeTextSize);	//Make the label large size.
			}
		    else if (currentGameboard.getWinner() == 'T') {
		    	xTurnLabel.setText("X");
		    	xTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
				oTurnLabel.setText("O");
				oTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
		    }
		}
		else {
			xTurnLabel.setText("X");	//Set labels to defaults.
			xTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
			oTurnLabel.setText("O");	//Set labels to defaults.
			oTurnLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, turnLabelDefaultTextSize);	//Make the label normal size.
		}
	}
	
	/**
	 * Updates the labels used to show the wins. 
	 */
	private void updateWinsLabels() {
		xWinsLabel.setText("Wins:  " + numOfXWins + "     " + getWinPercent(numOfXWins, numOfOWins) + "%");	//X wins label.
		tiedGamesLabel.setText("Tied:  " + numOfTied);	//Tied games label.
		oWinsLabel.setText("Wins:  " + numOfOWins + "     "+ getWinPercent(numOfOWins, numOfXWins) + "%");	//O wins label.
	}
	
	/**
	 * Updates the labels used to show the computer AI level. 
	 */
	private void updateAILevelLabels() {
		if (xPlayer.equals(PLAYER_USER))
			xLevelLabel.setText("Player");	//Label to indicate user is the player.
		else
			xLevelLabel.setText("Level " + computerLevelX);	//Label to indicate computer AI level.
		
		if (oPlayer.equals(PLAYER_USER))
			oLevelLabel.setText("Player");	//Label to indicate user is the player.
		else
			oLevelLabel.setText("Level " + computerLevelO);	//Label to indicate computer AI level.
	}
	
	/**
	 * @param numOfWins	Number of wins.
	 * @param numOfLosses	Number of losses.
	 * @return	Returns the win percentage (%) as an integer.
	 */
	private int getWinPercent(int numOfWins, int numOfLosses) {
		if (numOfLosses == 0) {
			if (numOfWins == 0)
				return(0);
			else
				return(100);
		}
		else
			return (100 * numOfWins / (numOfWins + numOfLosses));
	}
	
	/**
	 * Contains code for the XTURN game state.
	 * @throws InterruptedException Exception for timer delay in xTurnUser().
	 */
	private void xTurnState(){
	    if (xPlayer.equals(PLAYER_USER))
	    	xTurnUser();
	    else
	    	xTurnAI();
	}
	
	/**
	 * Contains code for the OTURN game state.
	 * @throws InterruptedException Exception for timer delay in oTurnUser().
	 */
	private void oTurnState() {
		if (oPlayer.equals(PLAYER_USER))
			oTurnUser();
		else
			oTurnAI();
	}
		
	/**
	 * Contains code for the WINNER game state.  Updates the computer levels.
	 * @throws InterruptedException 
	 */
	private void winnerState() {
		if (xAutomaticLevelSelection == true)		//Automatic level selection is true, so
			if (currentGameboard.getWinner() == 'X') {	//If the winner was X,
				if (computerLevelX > 1)						//Decrement the computer X level, but don't go below level 1.
					computerLevelX--;
			}
			else if (currentGameboard.getWinner() == 'O')	//X lost
				computerLevelX++;								//So increment X level.
		
		if (oAutomaticLevelSelection == true)		//Automatic level selection is true, so
			if (currentGameboard.getWinner() == 'O') {	//If the winner was O,
				if (computerLevelO > 1)						//Decrement the computer O level, but don't go below level 1.
					computerLevelO--;
			}
			else if (currentGameboard.getWinner() == 'X')	//O lost,
				computerLevelO++;								//So increment O level.

		try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}	//Wait 500 milliseconds until showing the prompt.
		promptForNextGame();															//Display the next game prompt.
		
		gameState = GameState.NEXT_GAME_CONFIRM;
	}

	/**
	 * Contains code for the NEXT_GAME_CONFIRM game state.
	 * @throws InterruptedException 
	 */
	private void nextGameConfirmState() {
		if (waitingForUserToClickNextGamePrompt == false) {	//Prompt was closed
			currentGameboard.newGame();							//So create a new game
			gameState = GameState.NEW_GAME;						//And go to the new game state.
		}
	}
	
	/**
	 * Generates a prompt indicating the end game status, and waits for the user to click the OK button.
	 */
	private void promptForNextGame() {
		waitingForUserToClickNextGamePrompt = true;	//Set to wait for the user to click the prompt.
		
		//Creates a runnable in the main UI thread to show the prompt.  Prompts have to be created in the UI thread.	
		runOnUiThread(new Runnable() {
	        @Override
			public void run()
	        {
	        	LayoutInflater li = LayoutInflater.from(GameActivity.this);
	        	endGamePromptContent = li.inflate(R.layout.end_game_prompt_layout, null);	//Create a context view for the prompt, so font and colors can be changed.
	        	endGamePromptWinnerLabel = (TextView) endGamePromptContent.findViewById(R.id.endGamePromptWinner);	//Label to indicate who the winner was in the end game prompt.
	        	endGamePromptNextXLevelLabel = (TextView) endGamePromptContent.findViewById(R.id.endGamePromptNextXLevel);	//Label to indicate the next computer X level in the end game prompt.
	        	endGamePromptNextOLevelLabel = (TextView) endGamePromptContent.findViewById(R.id.endGamePromptNextOLevel);	//Label to indicate the next computer O level in the end game prompt.
	        	endGamePromptButton = (Button) endGamePromptContent.findViewById(R.id.endGamePromptButton);	//Button to dismiss the next game prompt and continue.
	    		endGamePromptWinnerLabel.setTypeface(tf);		//Label to indicate who the winner was in the end game prompt.
	    		endGamePromptNextXLevelLabel.setTypeface(tf);	//Label to indicate the next computer X level in the end game prompt.
	    		endGamePromptNextOLevelLabel.setTypeface(tf);	//Label to indicate the next computer O level in the end game prompt.
	    		endGamePromptButton.setTypeface(tf);			//Button to dismiss the next game prompt and continue.
	    		updateEndGamePromptWinnerLabel(endGamePromptWinnerLabel);	//Update the prompt winner label with the winner message.
	    		updateEndGamePromptXLevelLabel(endGamePromptNextXLevelLabel);	//Update the prompt with the next X computer level message.
	    		updateEndGamePromptOLevelLabel(endGamePromptNextOLevelLabel);	//Update the prompt with the next O computer level message.
	    		
	        	endGamePromptDialog = new Dialog(GameActivity.this); 
	        	endGamePromptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        	endGamePromptDialog.setContentView(endGamePromptContent);
	        	endGamePromptDialog.show();
	        	
	        	endGamePromptDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
	        	{
	        	    @Override
					public void onDismiss(DialogInterface dialog)
	        	    {
	        	    	waitingForUserToClickNextGamePrompt = false;	//If the dialog is canceled, then continue on to the next game.
	        	    }
	        	});
	        		
	        	
	        }
	    });
		
	}
	
	/**
	 * Updates the winner text view to indicate who is the winner.  It also
	 * sets the text color to the winner's color.
	 * @param promptWinnerLabel	TextView to update.
	 */
	private void updateEndGamePromptWinnerLabel(TextView promptWinnerLabel) {
		int tiedGameColor = getResources().getColor(R.color.tie_color);	//Get the tied game color.
		int xColor = getResources().getColor(R.color.x_color);			//Get the X marker color.
		int oColor = getResources().getColor(R.color.o_color);			//Get the O marker color.
		
		//Update label based on the winner.
		if (currentGameboard.getWinner() == 'T') {						//Tied game
			promptWinnerLabel.setText("TIED GAME");						//So show tied game message
			promptWinnerLabel.setTextColor(tiedGameColor);				//And set the color to the tied game color
		}
		else if (xPlayer.equals(PLAYER_USER) && oPlayer.equals(PLAYER_COMPUTER)){	//User is X, and is playing the computer.
			if (currentGameboard.getWinner() == 'X') {					//User is the winner
				promptWinnerLabel.setText("YOU WIN!!!");					//So so the user winner message
				promptWinnerLabel.setTextColor(xColor);					//And set the color to the user's marker color
			}
			else {	//User lost
				promptWinnerLabel.setText("YOU LOSE");					//So show user lost message
				promptWinnerLabel.setTextColor(oColor);					//And set the color to the user's opponent's marker color.
			}
		}
		else if (xPlayer.equals(PLAYER_COMPUTER) && oPlayer.equals(PLAYER_USER)){	//User is O, and is playing the computer.
			if (currentGameboard.getWinner() == 'X') {					//User lost
				promptWinnerLabel.setText("YOU LOSE");					//So show user lost message
				promptWinnerLabel.setTextColor(xColor);					//And set the color to the user's opponent's marker color.
			}
			else {	//User is O and is the winner
				promptWinnerLabel.setText("YOU WIN!!!");				//So show user winner message
				promptWinnerLabel.setTextColor(oColor);					//And set the color to the user's marker color
			}
		}	
		else {
			if (currentGameboard.getWinner() == 'X') {					//X computer is the winner
				promptWinnerLabel.setText("X WINS!!!");					//So show that X is the winner
				promptWinnerLabel.setTextColor(xColor);					//And set color the X marker color
			}
			else {	//O computer is the winner
				promptWinnerLabel.setText("O WINS!!!");					//So show that O is the winner
				promptWinnerLabel.setTextColor(oColor);					//And set color the O marker color
			}
		}
	}
	
	/**
	 * If the X player is the computer, this method updates the TextView passed
	 * to it with a string indicating the next computer level for X.  Else, it
	 * updates the TextView with a blank string.
	 * @param endGamePromptNextXLevelLabel	The text view to be updated.
	 */
	private void updateEndGamePromptXLevelLabel(TextView endGamePromptNextXLevelLabel) {
		if (xPlayer.equals(PLAYER_COMPUTER) && oPlayer.equals(PLAYER_USER))
			endGamePromptNextXLevelLabel.setText("Next Level: " + computerLevelX);
		else if (xPlayer.equals(PLAYER_COMPUTER))
			endGamePromptNextXLevelLabel.setText("Next X Level: " + computerLevelX);	
		else
			endGamePromptNextXLevelLabel.setText("");
	}
	
	/**
	 * If the O player is the computer, this method updates the TextView passed
	 * to it with a string indicating the next computer level for O.  Else, it
	 * updates the TextView with a blank string.
	 * @param endGamePromptNextXLevelLabel	The text view to be updated.
	 */
	private void updateEndGamePromptOLevelLabel(TextView endGamePromptNextOLevelLabel) {
		if (oPlayer.equals(PLAYER_COMPUTER) && xPlayer.equals(PLAYER_USER))
			endGamePromptNextOLevelLabel.setText("Next Level: " + computerLevelO);
		else if (oPlayer.equals(PLAYER_COMPUTER))
			endGamePromptNextOLevelLabel.setText("Next O Level: " + computerLevelO);	
		else
			endGamePromptNextOLevelLabel.setText("");
	}
	
	/**
	 * Waits for the user to click a square, then updates that square with a
	 * 'X' if possible.  Else it will continue to wait.  If there is a winner,
	 * it jumps to the winner state.  Else is will jump the O's turn state.
	 * 
	 * @throws InterruptedException	Used for Thread.sleep().
	 */
 	private void xTurnUser() {
		int selectedColumn;
	    int selectedRow;
	    char gameSpaceState;
	    
	    userInterface.waitingForUser();	//Let the user interface code know that it is the users turn.
	    if (userInterface.userClicked() == true) {
	    	selectedColumn = userInterface.getUserSelectedColumn();	//Get the column the user clicked.
	    	selectedRow = userInterface.getUserSelectedRow();	//Get the row the user clicked.
	    	gameSpaceState = currentGameboard.getGameSpaceState(selectedColumn, selectedRow);	//Get the state of the square the user clicked.
	    	if (gameSpaceState == 'N') {	//If the square is empty, then fill it with an X.
	    		currentGameboard.updateGameSpace(selectedColumn, selectedRow, 'X');	//Update square with an X.
	    		if (currentGameboard.getWinner() == 'X') {
	    			numOfXWins++;
	    			gameState = GameState.WINNER;
	    		}
	    		else if (currentGameboard.getWinner() == 'T') {
	    			numOfTied++;
	    			gameState = GameState.WINNER;
	    		}
	    		else
	    			gameState = GameState.OTURN;
	    	}
	    }
	}
	
	/**
	 * Gets the computer's move, and updates the square with an 'X'.  If there
	 * is a winner, it jumps to the winner state.  Else is will jump the O's
	 * turn state.
	 */
	private void xTurnAI() {
		int selectedColumn;
	    int selectedRow;
	    
	    computerAI.generateComputerMove(currentGameboard, computerLevelX, 'X');
		selectedColumn = computerAI.getComputerMoveColumn();
	    selectedRow = computerAI.getComputerMoveRow();
		currentGameboard.updateGameSpace(selectedColumn, selectedRow, 'X');	//Update square with an X.
	    if (currentGameboard.getWinner() == 'X') {
	    	numOfXWins++;
	    	gameState = GameState.WINNER;
	    }
	    else if (currentGameboard.getWinner() == 'T') {
	    	numOfTied++;
	    	gameState = GameState.WINNER;
	    }
    	else
    		gameState = GameState.OTURN;
	}
	
	/**
	 * Waits for the user to click a square, then updates that square with an
	 * 'O' if possible.  Else it will continue to wait.  If there is a winner,
	 * it jumps to the winner state.  Else is will jump the X's turn state.
	 * 
	 * @throws InterruptedException	Used for Thread.sleep().
	 */
	private void oTurnUser() {
		int selectedColumn;
	    int selectedRow;
	    char gameSpaceState;
	    
	    userInterface.waitingForUser();	//Let the user interface code know that it is the users turn.
	    if (userInterface.userClicked() == true) {
	    	selectedColumn = userInterface.getUserSelectedColumn();	//Get the column the user clicked.
	    	selectedRow = userInterface.getUserSelectedRow();	//Get the row the user clicked.
	    	gameSpaceState = currentGameboard.getGameSpaceState(selectedColumn, selectedRow);	//Get the state of the square the user clicked.
	    	if (gameSpaceState == 'N') {	//If the square is empty, then fill it with an O.
	    		currentGameboard.updateGameSpace(selectedColumn, selectedRow, 'O');	//Update square with an O.
	    		if (currentGameboard.getWinner() == 'O') {
	    			numOfOWins++;
	    			gameState = GameState.WINNER;
	    		}
	    		else if (currentGameboard.getWinner() == 'T') {
	    			numOfTied++;
	    			gameState = GameState.WINNER;
	    		}
	    		else
	    			gameState = GameState.XTURN;
	    	}
	    }
	}
	
	/**
	 * Gets the computer's move, and updates the square with an 'O'.  If there
	 * is a winner, it jumps to the winner state.  Else is will jump the X's
	 * turn state.
	 */
	private void oTurnAI() {
		int selectedColumn;
	    int selectedRow;
	    
	    computerAI.generateComputerMove(currentGameboard, computerLevelO, 'O');
	    selectedColumn = computerAI.getComputerMoveColumn();
	    selectedRow = computerAI.getComputerMoveRow();
	    currentGameboard.updateGameSpace(selectedColumn, selectedRow, 'O');	//Update square with an O.
	    if (currentGameboard.getWinner() == 'O') {
	    	numOfOWins++;
	    	gameState = GameState.WINNER;
	    }
	    else if (currentGameboard.getWinner() == 'T') {
	    	numOfTied++;
	    	gameState = GameState.WINNER;
	    }
	    else
	    	gameState = GameState.XTURN;
	}
	
}
